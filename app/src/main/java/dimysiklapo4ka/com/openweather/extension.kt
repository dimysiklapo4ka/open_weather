package dimysiklapo4ka.com.openweather

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity

private val celsium = '\u2103'

fun String.addCelsium(): String = if(this.isBlank()) "" else "$this $celsium"

fun FragmentActivity.replaceFragment(fragment: Fragment,argument: Bundle?, needToBackStack: Boolean = true){
    supportFragmentManager.beginTransaction().apply {
        argument?.let { fragment.arguments = it }
        this.replace(R.id.container, fragment, fragment::class.java.simpleName)
        if (needToBackStack) {
            addToBackStack(fragment::class.java.simpleName)
        }
    }.commit()
}

fun FragmentActivity.backStack(){
    supportFragmentManager.popBackStack()
}

fun <E> Iterable<E>.replace(old: E, new: E) = map { if (it == old) new else it }
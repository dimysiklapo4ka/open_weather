package dimysiklapo4ka.com.openweather.application

import android.app.Application
import dimysiklapo4ka.com.data.di.dataInjector
import dimysiklapo4ka.com.domain.di.domainInjector
import dimysiklapo4ka.com.openweather.di.appInjector
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class WeatherApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        val koinModules = listOf(appInjector, dataInjector, domainInjector)
        startKoin {
            androidLogger()
            androidContext(this@WeatherApplication)
            modules(koinModules)
        }
    }

}
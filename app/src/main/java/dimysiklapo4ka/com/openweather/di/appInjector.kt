package dimysiklapo4ka.com.openweather.di

import dimysiklapo4ka.com.openweather.ui.fragment.add.AddViewModel
import dimysiklapo4ka.com.openweather.ui.fragment.main.MainViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appInjector = module {
    viewModel { MainViewModel(get()) }
    viewModel { AddViewModel(get()) }
}
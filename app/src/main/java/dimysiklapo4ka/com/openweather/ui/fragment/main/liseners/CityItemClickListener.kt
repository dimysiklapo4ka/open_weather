package dimysiklapo4ka.com.openweather.ui.fragment.main.liseners

import dimysiklapo4ka.com.domain.models.WeatherModel

interface CityItemClickListener {
    fun itemClick(weatherModel: WeatherModel, positionUpdate: Int)
    fun itemLongClick(weatherModel: WeatherModel, positionUpdate: Int)
}
package dimysiklapo4ka.com.openweather.ui.base

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import dimysiklapo4ka.com.openweather.R

abstract class BaseFragment<VM: BaseViewModel>: Fragment(), SwipeRefreshLayout.OnRefreshListener  {
    protected abstract val resLayout: Int
    protected abstract val viewModel: VM

    protected abstract val needToSwipeUpdate: Boolean
    protected lateinit var swipeRefreshLayout: SwipeRefreshLayout

    private val errorObserver = Observer<String>{ Toast.makeText(activity, it, Toast.LENGTH_SHORT).show() }
    private val loadingObserver = Observer<Boolean> {  }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observe(viewModel)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        observeSwipeToUpdate(resLayout)

    private fun observeSwipeToUpdate(resLayout: Int): View {
        swipeRefreshLayout = SwipeRefreshLayout(activity!!)
        val view = layoutInflater.inflate(resLayout, null)
        swipeRefreshLayout.let {
            it.layoutParams =
                ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            it.addView(view)
            it.isEnabled = needToSwipeUpdate
            it.setOnRefreshListener(this)
            it.setColorSchemeResources(
                R.color.colorAccent,
                R.color.colorPrimary,
                R.color.colorPrimaryDark
            )
        }
        return swipeRefreshLayout
    }

    override fun onRefresh() {
        refresh()
    }

    protected abstract fun refresh()

    protected abstract fun observeLiveData(viewModel: VM)

    private fun observe(viewModel: VM){
        observeLiveData(viewModel)
        with(viewModel){
            errorLiveData.observe(this@BaseFragment, errorObserver)
            loadingLiveData.observe(this@BaseFragment, loadingObserver)
        }
    }

    fun <P, N> alertDialogShow(title: String, message: String?,
                               positiveButtonText: String?,
                               positiveClick: DialogInterface.() -> P,
                               negativeButtonText: String?,
                               negativeClick: DialogInterface.() -> N){

        AlertDialog.Builder(activity!!)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(positiveButtonText) { dialog, _ -> positiveClick.invoke(dialog) }
            .setNegativeButton(negativeButtonText) { dialog, _ -> negativeClick.invoke(dialog) }
            .show()
    }
}
package dimysiklapo4ka.com.openweather.ui.fragment.add

import androidx.lifecycle.MutableLiveData
import dimysiklapo4ka.com.domain.interactors.WeatherInteractor
import dimysiklapo4ka.com.domain.models.StatusResponse
import dimysiklapo4ka.com.domain.models.WeatherModel
import dimysiklapo4ka.com.openweather.ui.base.BaseViewModel

class AddViewModel(private val weatherInteractor: WeatherInteractor): BaseViewModel() {

    val addLiveData =  MutableLiveData<WeatherModel>()

    fun addCity(city: String){
        doAsyncWork {
            val job = weatherInteractor.getWeather(city)
            when(job?.status){
                StatusResponse.ERROR -> { errorLiveData.postValue(job.error) }
                StatusResponse.SUCCESS -> {
                    job.response?.let { addLiveData.postValue(it) }
                }
                else -> { errorLiveData.postValue("Check Internet") }
            }
        }
    }
}
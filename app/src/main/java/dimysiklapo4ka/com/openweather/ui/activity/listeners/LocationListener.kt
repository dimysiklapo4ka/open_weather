package dimysiklapo4ka.com.openweather.ui.activity.listeners

interface LocationListener {
    fun updateLocation(lat: Double, lon: Double)
}
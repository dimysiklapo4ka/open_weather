package dimysiklapo4ka.com.openweather.ui.activity

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.net.Uri
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import dimysiklapo4ka.com.openweather.R
import dimysiklapo4ka.com.openweather.replaceFragment
import dimysiklapo4ka.com.openweather.ui.activity.listeners.LocationListener
import dimysiklapo4ka.com.openweather.ui.fragment.main.MainFragment
import locationprovider.davidserrano.com.LocationProvider

class MainActivity : AppCompatActivity() {

    private lateinit var locationProvider: LocationProvider
    private lateinit var locationManager: LocationManager
    private val SETTINGS_ACTION = 123

    private var locationListener: LocationListener? = null

    private val locationCallback = object:LocationProvider.LocationCallback{
        override fun onNewLocationAvailable(p0: Float, p1: Float) {
            Log.e("LOCATION", "onNewLocationAvailable")
            locationListener?.updateLocation(p0.toDouble(),p1.toDouble())
        }

        override fun updateLocationInBackground(p0: Float, p1: Float) {
            Log.e("LOCATION", "updateLocationInBackground")
        }

        override fun locationServicesNotEnabled() {
            Log.e("LOCATION", "locationServicesNotEnabled")
        }

        override fun locationRequestStopped() {
            Log.e("LOCATION", "locationRequestStopped")
        }

        override fun networkListenerInitialised() {
            Log.e("LOCATION", "networkListenerInitialised")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        replaceFragment(MainFragment(), null, false)
    }

    override fun onDestroy() {
        super.onDestroy()
        lifecycle.removeObserver(locationProvider)
    }

    fun setOnLocationListener(listener: LocationListener){
        locationListener = listener
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationProvider = LocationProvider.Builder()
            .setContext(this)
            .setListener(locationCallback)
            .create()
        requestPermission()
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
            showExplanation()
    }

    private fun requestPermission(){
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
            &&  ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            getMyLocation()
        }else
            ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION), 2000)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            2000 -> {
                if ((grantResults.isNotEmpty() &&
                            grantResults[0] == PackageManager.PERMISSION_GRANTED) &&
                    grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    getMyLocation()
                } else {
                    alertDialogShow("Permission", "Access Permission", "Request Permissions", {
                        requestPermission()
                        this.dismiss()
                    },"",{})

                }
                return
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == SETTINGS_ACTION && resultCode == Activity.RESULT_OK) {
            getMyLocation()
        }
    }

    private fun getMyLocation(){
        Log.e("LOCATION", "getMyLocation")
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
            &&  ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            lifecycle.addObserver(locationProvider)
            locationProvider.requestLocation()
        }
    }

    private fun <P, N> alertDialogShow(title: String, message: String?,
                                       positiveButtonText: String?,
                                       positiveClick: DialogInterface.() -> P,
                                       negativeButtonText: String?,
                                       negativeClick: DialogInterface.() -> N){

        AlertDialog.Builder(this)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(positiveButtonText) { dialog, _ -> positiveClick.invoke(dialog) }
            .setNegativeButton(negativeButtonText) { dialog, _ -> negativeClick.invoke(dialog) }
            .show()
    }

    private fun showExplanation() {
        alertDialogShow("Enable GPS", "Need Enabled GPS", "Enable", {
            this.dismiss()
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            val uri = Uri.fromParts("package", packageName, null)
            intent.data = uri
            startActivityForResult(intent, SETTINGS_ACTION)
        }, "Cancel", {this.dismiss()})
    }

}

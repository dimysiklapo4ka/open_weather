package dimysiklapo4ka.com.openweather.ui.fragment.add

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import dimysiklapo4ka.com.domain.models.WeatherModel
import dimysiklapo4ka.com.openweather.R
import dimysiklapo4ka.com.openweather.backStack
import dimysiklapo4ka.com.openweather.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_add.*
import org.koin.android.ext.android.inject

class AddFragment: BaseFragment<AddViewModel>() {
    override val resLayout: Int = R.layout.fragment_add
    override val viewModel: AddViewModel by inject()
    override val needToSwipeUpdate: Boolean = false

    private val addObserver = Observer<WeatherModel>{
        activity?.backStack()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btAddCity.setOnClickListener {
            if (!etAddCity.text.isNullOrEmpty())
                viewModel.addCity(etAddCity.text.toString())
            else
                Toast.makeText(activity, "Try again", Toast.LENGTH_SHORT).show()
        }
    }

    override fun refresh() {}

    override fun observeLiveData(viewModel: AddViewModel) {
        with(viewModel){
            addLiveData.observe(this@AddFragment, addObserver)
        }
    }

}
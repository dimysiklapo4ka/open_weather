package dimysiklapo4ka.com.openweather.ui.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

abstract class BaseViewModel : ViewModel(){
    val IO = Dispatchers.IO
    val loadingLiveData = MutableLiveData<Boolean>()
    val errorLiveData = MutableLiveData<String>()

    fun <P> doAsyncWork(doWork: suspend CoroutineScope.() -> P){
        CoroutineScope(IO).launch {
            withContext(this.coroutineContext){
                doWork.invoke(this)
            }
        }
    }
}
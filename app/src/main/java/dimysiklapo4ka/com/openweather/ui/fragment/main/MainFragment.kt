package dimysiklapo4ka.com.openweather.ui.fragment.main

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import dimysiklapo4ka.com.domain.models.WeatherModel
import dimysiklapo4ka.com.openweather.R
import dimysiklapo4ka.com.openweather.replaceFragment
import dimysiklapo4ka.com.openweather.ui.activity.MainActivity
import dimysiklapo4ka.com.openweather.ui.activity.listeners.LocationListener
import dimysiklapo4ka.com.openweather.ui.base.BaseFragment
import dimysiklapo4ka.com.openweather.ui.fragment.add.AddFragment
import dimysiklapo4ka.com.openweather.ui.fragment.main.adapter.WeatherAdapter
import dimysiklapo4ka.com.openweather.ui.fragment.main.liseners.CityItemClickListener
import dimysiklapo4ka.com.openweather.ui.fragment.map.MAP_MODEl
import dimysiklapo4ka.com.openweather.ui.fragment.map.MapsFragment
import kotlinx.android.synthetic.main.fragment_main.*
import org.koin.android.ext.android.inject

class MainFragment: BaseFragment<MainViewModel>(), CityItemClickListener, LocationListener{
    override val resLayout: Int = R.layout.fragment_main
    override val viewModel: MainViewModel by inject()
    override val needToSwipeUpdate: Boolean = false
    private val adapter = WeatherAdapter(this)

    private val cityObserver = Observer<List<WeatherModel>>{
        adapter.add(it)
        swipeRefreshLayout.isRefreshing = false
    }

    private val updateObserver = Observer<Map<Int, WeatherModel>>{
        adapter.update(it.keys.first(), it.values.first() )
    }

    private val deleteObserver = Observer<Map<Int, Boolean>> {
        adapter.delete(it.keys.first())
    }

    private val locationObserver = Observer<WeatherModel> {
        adapter.updateLocation(0, it)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity!! as MainActivity).setOnLocationListener(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getAllCity()
        rvMain.adapter = adapter
        btAdd.setOnClickListener {
            activity?.replaceFragment(AddFragment(),null, true)
        }
    }

    override fun refresh() {
//        viewModel.getAllCity()
    }

    override fun observeLiveData(viewModel: MainViewModel) {
        with(viewModel){
            cityLiveData.observe(this@MainFragment, cityObserver)
            updateLiveData.observe(this@MainFragment, updateObserver)
            deletedLiveData.observe(this@MainFragment, deleteObserver)
            locationLiveData.observe(this@MainFragment, locationObserver)
        }
    }

    override fun itemClick(weatherModel: WeatherModel, positionUpdate: Int) {
        alertDialogShow("Choose your action", null, "Update", {
            viewModel.updateCity(weatherModel.name, positionUpdate)
            this.dismiss()
        },"See to map", {
            val bundle = Bundle()
            bundle.putSerializable(MAP_MODEl, weatherModel)
            activity!!.replaceFragment(MapsFragment(), bundle, true)
            this.dismiss()
        })
    }

    override fun itemLongClick(weatherModel: WeatherModel, positionUpdate: Int) {
        alertDialogShow("Delete city", weatherModel.name, "Delete", {
            viewModel.deleteCity(weatherModel, positionUpdate)
            this.dismiss()
        },"", {})
    }

    override fun updateLocation(lat: Double, lon: Double) {
        viewModel.getCityLocation(lat, lon)
    }
}
package dimysiklapo4ka.com.openweather.ui.fragment.map

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.*

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import dimysiklapo4ka.com.domain.models.WeatherModel
import dimysiklapo4ka.com.openweather.R
import dimysiklapo4ka.com.openweather.addCelsium
import dimysiklapo4ka.com.openweather.backStack
import kotlinx.android.synthetic.main.fragment_maps.*

const val MAP_MODEl = "GMAP_MODEL"

class MapsFragment : Fragment(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private var markerModel: WeatherModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let { markerModel = it.getSerializable(MAP_MODEl) as WeatherModel? }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_maps, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (markerModel == null) activity!!.backStack()

        map.onCreate(savedInstanceState)
        map.onResume()

        MapsInitializer.initialize(view.context)
        map.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        markerModel?.let {
            val marker = LatLng(it.lat, it.lon)
            mMap.addMarker(MarkerOptions()
                .position(marker)
                .title(it.name)
                .snippet(it.temperature.toString().addCelsium())
            )
            mMap.moveCamera(CameraUpdateFactory
                .newLatLngZoom(marker, 8f)
            )
        }

    }


}

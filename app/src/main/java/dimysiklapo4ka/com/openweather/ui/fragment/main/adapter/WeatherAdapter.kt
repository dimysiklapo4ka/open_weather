package dimysiklapo4ka.com.openweather.ui.fragment.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import dimysiklapo4ka.com.data.currentDate
import dimysiklapo4ka.com.domain.models.WeatherModel
import dimysiklapo4ka.com.openweather.R
import dimysiklapo4ka.com.openweather.addCelsium
import dimysiklapo4ka.com.openweather.ui.fragment.main.liseners.CityItemClickListener
import kotlinx.android.synthetic.main.view_city_item.view.*
import java.util.*

class WeatherAdapter(private val clickListener: CityItemClickListener): RecyclerView.Adapter<WeatherAdapter.WeatherViewHolder>() {

    private val cities = mutableListOf<WeatherModel>()

    fun add(city: List<WeatherModel>){
        cities.clear()
        cities.add(0, WeatherModel(0.0,0.0, "Location", "10d", 10, String().currentDate(Date())))
        cities.addAll(city)
        notifyDataSetChanged()
    }

    fun update(position: Int, weatherModel: WeatherModel){
        cities[position] = weatherModel
        notifyItemChanged(position)
    }

    fun updateLocation(position: Int, weatherModel: WeatherModel){
        if (cities.isEmpty())
            cities.add(0, WeatherModel(0.0,0.0, "Location", "10d", 10, String().currentDate(Date())))
        cities[position] = weatherModel
        notifyItemChanged(position)
    }

    fun delete(position: Int){
        cities.removeAt(position)
        notifyItemRemoved(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherViewHolder =
        WeatherViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_city_item, parent, false))

    override fun getItemCount(): Int = cities.size

    override fun onBindViewHolder(holder: WeatherViewHolder, position: Int) {
        holder.bindView(cities[position])
    }

    inner class WeatherViewHolder(view: View): RecyclerView.ViewHolder(view){

        fun bindView(weatherModel: WeatherModel){
            bind(weatherModel)
            if (adapterPosition != 0) {
                itemView.setOnClickListener {
                    clickListener.itemClick(weatherModel, adapterPosition)
                }
                if (adapterPosition != 1) {
                    itemView.setOnLongClickListener {
                        clickListener.itemLongClick(weatherModel, adapterPosition)
                        true
                    }
                }
            }
        }

        private fun bind(weatherModel: WeatherModel){
            itemView.tvCity.text = weatherModel.name
            itemView.tvTemperature.text = weatherModel.temperature.toString().addCelsium()
            Glide.with(itemView.context)
                .load(String.format(itemView.context.getString(R.string.icon_url), weatherModel.icon))
//                .placeholder(R.drawable.ic_launcher_background)
                .into(itemView.ivWeatherImage)
        }
    }
}
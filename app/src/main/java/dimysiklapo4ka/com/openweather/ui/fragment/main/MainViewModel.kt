package dimysiklapo4ka.com.openweather.ui.fragment.main

import androidx.lifecycle.MutableLiveData
import dimysiklapo4ka.com.data.currentDate
import dimysiklapo4ka.com.domain.interactors.WeatherInteractor
import dimysiklapo4ka.com.domain.models.StatusResponse
import dimysiklapo4ka.com.domain.models.WeatherModel
import dimysiklapo4ka.com.openweather.ui.base.BaseViewModel
import java.util.*

class MainViewModel(private val weatherInteractor: WeatherInteractor): BaseViewModel() {

    val cityLiveData = MutableLiveData<List<WeatherModel>>()
    val updateLiveData = MutableLiveData<Map<Int,WeatherModel>>()
    val deletedLiveData = MutableLiveData<Map<Int,Boolean>>()
    val locationLiveData = MutableLiveData<WeatherModel>()

    fun getAllCity(){
        loadingLiveData.postValue(true)
        doAsyncWork {
            val weathers = weatherInteractor.getAllWeatherData()
            if (weathers.isNullOrEmpty()) {
                val job = weatherInteractor.getWeather("Dnipropetrovsk")
                when (job?.status) {
                    StatusResponse.ERROR -> {
                        errorLiveData.postValue(job.error)
                        cityLiveData.postValue(listOf(WeatherModel(48.45, 34.98, "Dnipropetrovsk",
                            "01d", 23, String().currentDate(Date()))))
                    }
                    StatusResponse.SUCCESS -> {
                        job.response?.let { cityLiveData.postValue(listOf(it)) }
                    }
                    else -> {
                        errorLiveData.postValue("Check Internet")
                        cityLiveData.postValue(listOf(WeatherModel(48.45, 34.98, "Dnipropetrovsk",
                            "01d", 23, String().currentDate(Date()))))
                    }
                }
            }
            cityLiveData.postValue(weathers)
            loadingLiveData.postValue(false)
        }
    }

    fun getCityLocation(lat: Double, lon: Double){
        doAsyncWork {
            val job = weatherInteractor.getWeather(lat, lon)
            when(job?.status){
                StatusResponse.ERROR -> { errorLiveData.postValue(job.error) }
                StatusResponse.SUCCESS -> {
                    job.response?.let { locationLiveData.postValue(it) }
                }
                else -> { errorLiveData.postValue("Check Internet") }
            }
        }
    }

    fun updateCity(city: String, position: Int){
        doAsyncWork {
            val job = weatherInteractor.getWeather(city)
            when(job?.status){
                StatusResponse.ERROR -> { errorLiveData.postValue(job.error) }
                StatusResponse.SUCCESS -> {
                    job.response?.let { updateLiveData.postValue(mapOf(Pair(position, it))) }
                }
                else -> { errorLiveData.postValue("Check Internet") }
            }
        }
    }

    fun deleteCity(meatherModel: WeatherModel, position: Int){
        doAsyncWork {
            weatherInteractor.delete(meatherModel)
            deletedLiveData.postValue(mapOf(Pair(position, true)))
        }
    }

}
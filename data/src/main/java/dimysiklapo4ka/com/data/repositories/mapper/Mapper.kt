package dimysiklapo4ka.com.data.repositories.mapper

import com.google.gson.Gson
import dimysiklapo4ka.com.data.currentDate
import dimysiklapo4ka.com.data.datasources.api.model.CurrentWeatherResponse
import dimysiklapo4ka.com.data.datasources.database.entities.WeatherEntity
import dimysiklapo4ka.com.domain.models.WeatherModel
import java.util.*

internal object Mapper {

    private val gson = Gson()

    internal fun currentWeatherResponseToWeatherEntity(currentWeatherResponse: CurrentWeatherResponse): WeatherEntity = WeatherEntity(
            currentWeatherResponse.coord.lat,
            currentWeatherResponse.coord.lon,
            currentWeatherResponse.name,
            currentWeatherResponse.weather[0].icon,
            currentWeatherResponse.main.temp.toInt(),
            String().currentDate(Date())
            )

    internal fun currentWeatherResponseToWeatherModel(currentWeatherResponse: CurrentWeatherResponse): WeatherModel = WeatherModel(
            currentWeatherResponse.coord.lat,
            currentWeatherResponse.coord.lon,
            currentWeatherResponse.name,
            currentWeatherResponse.weather[0].icon,
            currentWeatherResponse.main.temp.toInt(),
            String().currentDate(Date())
        )

    internal fun listWeatherEntityToListWeatherModel(weatherEntity: List<WeatherEntity>): List<WeatherModel> {
        val wetherModels = mutableListOf<WeatherModel>()
        return weatherEntity.mapTo(wetherModels, {weatherEntityToWeatherModel(it)})
    }

    internal fun weatherEntityToWeatherModel(weatherEntity: WeatherEntity): WeatherModel =
        gson.fromJson(gson.toJsonTree(weatherEntity), WeatherModel::class.java)

    internal fun weatherModelToWeatherEntity(wetherModel: WeatherModel): WeatherEntity =
        gson.fromJson(gson.toJsonTree(wetherModel), WeatherEntity::class.java)

}
package dimysiklapo4ka.com.data.repositories

import android.util.Log
import dimysiklapo4ka.com.data.BuildConfig
import dimysiklapo4ka.com.data.datasources.api.WeatherApi
import dimysiklapo4ka.com.data.datasources.api.model.CurrentWeatherResponse
import dimysiklapo4ka.com.data.datasources.database.WeatherDatabase
import dimysiklapo4ka.com.data.repositories.mapper.Mapper
import dimysiklapo4ka.com.domain.geteways.WeatherGeteway
import dimysiklapo4ka.com.domain.models.DataResponse
import dimysiklapo4ka.com.domain.models.StatusResponse
import dimysiklapo4ka.com.domain.models.WeatherModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.coroutines.Dispatchers.IO
import retrofit2.HttpException

internal class WeatherRepository(
    database: WeatherDatabase,
    private val api: WeatherApi
): WeatherGeteway {

    private val db = database.getWeatherDao()
    private val mapper = Mapper

    private fun insert(weatherResponse: CurrentWeatherResponse){
        db.insert(mapper.currentWeatherResponseToWeatherEntity(weatherResponse))
    }

    override suspend fun getWeather(city: String): DataResponse? {
        var response: DataResponse? = null
        val job = CoroutineScope(IO).launch {
            val weatherResponse = api.getWeatherFromCityName(cityName = city,
                apiKey = BuildConfig.API_KEY)
            withContext(IO){
                try {
                    val currentWeatherResponse = weatherResponse.await()
                    if (currentWeatherResponse.isSuccessful){
                        response = DataResponse(StatusResponse.SUCCESS,
                            currentWeatherResponse.body()?.let { mapper.currentWeatherResponseToWeatherModel(it) })
                        currentWeatherResponse.body()?.let { insert(it) }
                    }else
                        response = DataResponse(StatusResponse.ERROR, error = currentWeatherResponse.message())

                    Log.e("WEATHER", currentWeatherResponse.message())
                    Log.e("WEATHER", currentWeatherResponse.errorBody().toString())
                    Log.e("WEATHER", currentWeatherResponse.body().toString())
                }catch (http: HttpException){
                    Log.e("WEATHER ERROR", http.message())
                    response = DataResponse(StatusResponse.ERROR, error = "Oups, please update later")
                }catch (e: Exception){
                    Log.e("WEATHER ERROR", e.message)
                    response = DataResponse(StatusResponse.ERROR, error = "Oups, please update later")
                }
            }
        }
        job.join()
        return response
    }

    override suspend fun getWeather(lat: Double, lon: Double): DataResponse?{
        var response: DataResponse? = null
        val job = CoroutineScope(IO).launch {
            val weatherResponse = api.getWeatherFromLocation(lat = lat,
                lon = lon, apiKey = BuildConfig.API_KEY)
            withContext(IO){
                try {
                    val currentWeatherResponse = weatherResponse.await()
                    if (currentWeatherResponse.isSuccessful){
                        response = DataResponse(StatusResponse.SUCCESS,
                            currentWeatherResponse.body()?.let { mapper.currentWeatherResponseToWeatherModel(it) })
                    }else
                        response = DataResponse(StatusResponse.ERROR, error = currentWeatherResponse.message())
                }catch (http: HttpException){
                    response = DataResponse(StatusResponse.ERROR, error = "Oups, please update later")
                }catch (e: Exception){
                    response = DataResponse(StatusResponse.ERROR, error = "Oups, please update later")
                }
            }
        }
        job.join()
        return response
    }

    override suspend fun delete(weatherData: WeatherModel) {
        db.delete(mapper.weatherModelToWeatherEntity(weatherData))
    }

    override suspend fun getAllWeatherData(): List<WeatherModel>? {
        return mapper.listWeatherEntityToListWeatherModel(db.getAllWeatherData())
    }

    override suspend fun clearTable() {
        db.nukeTable()
    }

}
package dimysiklapo4ka.com.data.datasources.database.dao

import androidx.room.*
import dimysiklapo4ka.com.data.datasources.database.entities.WeatherEntity

@Dao
internal interface WeatherDao {
    //Added WeatherEntity in Database
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(weatherData: WeatherEntity)
    //Delete WeatherEntity of Database
    @Delete
    fun delete(weatherData: WeatherEntity)
    //Get all WeatherEntity of Database
    @Query("SELECT * FROM weather")
    fun getAllWeatherData(): List<WeatherEntity>
    //Delete All items in Database
    @Query("DELETE FROM weather")
    fun nukeTable()

}
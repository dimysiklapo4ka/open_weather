package dimysiklapo4ka.com.data.datasources.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import dimysiklapo4ka.com.data.di.DataBase

@Entity(tableName = DataBase.WEATHER_TABLE_NAME)
internal data class WeatherEntity(
    val lat: Double,
    val lon: Double,
    @PrimaryKey
    val name: String,
    val icon: String,
    val temperature: Int,
    val date: String
)
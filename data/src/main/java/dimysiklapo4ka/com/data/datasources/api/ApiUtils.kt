package dimysiklapo4ka.com.data.datasources.api

const val CURRENT_WEATHER = "weather"
const val HOURLY_WEATHER = "forecast/hourly"
const val DAYS_WEATHER = "forecast/daily"

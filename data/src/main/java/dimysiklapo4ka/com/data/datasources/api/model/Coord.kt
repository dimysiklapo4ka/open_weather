package dimysiklapo4ka.com.data.datasources.api.model

data class Coord(
    val lat: Double,
    val lon: Double
)
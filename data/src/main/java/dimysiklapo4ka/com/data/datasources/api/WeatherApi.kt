package dimysiklapo4ka.com.data.datasources.api

import dimysiklapo4ka.com.data.datasources.api.model.CurrentWeatherResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {
    @GET(CURRENT_WEATHER)
    fun getWeatherFromCityName(
        @Query("q")cityName: String,
        @Query("units")units: String = "metric",
        @Query("lang")language: String = "ru",
        @Query("appid")apiKey: String):
            Deferred<Response<CurrentWeatherResponse>>

    @GET(CURRENT_WEATHER)
    fun getWeatherFromLocation(
        @Query("lat")lat: Double,
        @Query("lon")lon: Double,
        @Query("units")units: String = "metric",
        @Query("lang")language: String = "ru",
        @Query("appid")apiKey: String):
            Deferred<Response<CurrentWeatherResponse>>
}
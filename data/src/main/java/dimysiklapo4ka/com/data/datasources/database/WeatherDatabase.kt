package dimysiklapo4ka.com.data.datasources.database

import androidx.room.Database
import androidx.room.RoomDatabase
import dimysiklapo4ka.com.data.datasources.database.dao.WeatherDao
import dimysiklapo4ka.com.data.datasources.database.entities.WeatherEntity

@Database(
    entities = [WeatherEntity::class],
    version = 1
)
internal abstract class WeatherDatabase: RoomDatabase() {
    abstract fun getWeatherDao(): WeatherDao
}
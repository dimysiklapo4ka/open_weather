package dimysiklapo4ka.com.data.datasources.api.model

data class Wind(
    val deg: Double,
    val speed: Double
)
package dimysiklapo4ka.com.data.datasources.api

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import dimysiklapo4ka.com.data.BuildConfig
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitClient{

    fun getWeatherApi(): WeatherApi{
        return retrofitClient.create(WeatherApi::class.java)
    }

    companion object{
        private val client = OkHttpClient()
        private val retrofitClient: Retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory.invoke())
            .build()
    }
}
package dimysiklapo4ka.com.data.di

import androidx.room.Room
import dimysiklapo4ka.com.data.di.DataBase.DB_NAME
import dimysiklapo4ka.com.data.datasources.api.RetrofitClient
import dimysiklapo4ka.com.data.datasources.database.WeatherDatabase
import dimysiklapo4ka.com.data.repositories.WeatherRepository
import dimysiklapo4ka.com.domain.geteways.WeatherGeteway
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

object DataBase {
    const val DB_NAME = "weather.db"
    const val WEATHER_TABLE_NAME = "weather"
}

val dataInjector = module {
    single { Room.databaseBuilder(androidApplication(), WeatherDatabase::class.java, DB_NAME).build() }
    single { get<WeatherDatabase>().getWeatherDao() }

    single { RetrofitClient().getWeatherApi() }
    single<WeatherGeteway>{WeatherRepository(get(), get())}
}
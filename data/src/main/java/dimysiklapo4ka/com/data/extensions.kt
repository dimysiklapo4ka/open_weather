package dimysiklapo4ka.com.data

import java.text.SimpleDateFormat
import java.util.*

fun String.currentDate(date: Date):String{
    val sdf = SimpleDateFormat("yyyy-MM-dd HH", Locale.getDefault())
    return sdf.format(date)
}
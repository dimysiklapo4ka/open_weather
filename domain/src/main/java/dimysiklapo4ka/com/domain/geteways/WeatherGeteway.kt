package dimysiklapo4ka.com.domain.geteways

import dimysiklapo4ka.com.domain.models.DataResponse
import dimysiklapo4ka.com.domain.models.WeatherModel

interface WeatherGeteway {
    suspend fun getWeather(city: String): DataResponse?
    suspend fun getWeather(lat: Double, lon: Double): DataResponse?
    suspend fun delete(weatherData: WeatherModel)
    suspend fun getAllWeatherData(): List<WeatherModel>?
    suspend fun clearTable()
}
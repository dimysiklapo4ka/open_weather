package dimysiklapo4ka.com.domain.interactors

import dimysiklapo4ka.com.domain.geteways.WeatherGeteway
import dimysiklapo4ka.com.domain.models.DataResponse
import dimysiklapo4ka.com.domain.models.WeatherModel

class WeatherInteractorImpl(val weatherGeteway: WeatherGeteway): WeatherInteractor {
    override suspend fun getWeather(city: String): DataResponse? {
        return weatherGeteway.getWeather(city)
    }

    override suspend fun getWeather(lat: Double, lon: Double): DataResponse? {
        return weatherGeteway.getWeather(lat, lon)
    }

    override suspend fun delete(weatherData: WeatherModel) {
        weatherGeteway.delete(weatherData)
    }

    override suspend fun getAllWeatherData(): List<WeatherModel>? {
        return weatherGeteway.getAllWeatherData()
    }

    override suspend fun clearTable() {
        weatherGeteway.clearTable()
    }
}
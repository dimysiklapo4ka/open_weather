package dimysiklapo4ka.com.domain.di

import dimysiklapo4ka.com.domain.interactors.WeatherInteractor
import dimysiklapo4ka.com.domain.interactors.WeatherInteractorImpl
import org.koin.dsl.module

val domainInjector = module {
    single <WeatherInteractor>{ WeatherInteractorImpl(get()) }
}
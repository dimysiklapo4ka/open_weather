package dimysiklapo4ka.com.domain.models

data class DataResponse(
    val status: StatusResponse,
    val response: WeatherModel? = null,
    val error: String? = null
)

enum class StatusResponse {
    LOADING,
    SUCCESS,
    ERROR
}
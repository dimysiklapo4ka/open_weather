package dimysiklapo4ka.com.domain.models

import java.io.Serializable

data class WeatherModel(
    val lat: Double,
    val lon: Double,
    val name: String,
    val icon: String,
    val temperature: Int,
    val date: String
): Serializable